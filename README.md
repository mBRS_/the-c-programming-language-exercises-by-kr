# The C Programming Language Exercises by K&R

Solved exercises taken from the famous book "The C programming language" by Brian Kernighan and Dennis Ritchie. This book is considered a classic of the CS literature and the "go to" book for many years in regards to C standard. These are just my (a humble CS student) solutions of it's exercises that I did while reading it.

Some consecutive exercises are grouped in a single file because they are very similar or have some sort of relations between them.


