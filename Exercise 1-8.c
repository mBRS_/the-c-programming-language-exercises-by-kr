#include <stdio.h>
/* count lines in input */
int main()
{
    int c, nl,nb,nt;
    nl = nb = nt =  0;
    while ((c = getchar()) != EOF) {
        if (c == '\n') 
            ++nl;
        if (c == ' ')
            ++nb;
        if (c == '\t')
            ++nt;
    }
    printf("new lines= %d, tabs = %d, blanks = %d\n", nl,nt,nb);
}