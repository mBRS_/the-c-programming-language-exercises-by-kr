#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */

int getline1(char line[], int maxline);
void copy(char to[], char from[]);
int strip(char line[], int len);


/* print the longest input */
int main()
{
    int len;
    char line[MAXLINE];
    while ((len = getline1(line, MAXLINE)) > 0){
        len=strip(line,len);
        if (len>0){
            printf("%s\n", line);
            printf("%d\n", len);
        }
    }
    return 0;
    }

/* getline: read a line into s, return length */
int getline1(char s[],int lim)
{
    int c, i;
    for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i;
    i = 0;
    while ((to[i] = from[i]) != '\0')
    ++i;
}

int strip(char line[], int len) {
    for( ;len>0;--len){
        if(line[len-1]==' ' || line[len-1]=='\t' || line[len-1]=='\n'){
            line[len-1]='\0';
        }
        else break;
    }
    
    return len;
}