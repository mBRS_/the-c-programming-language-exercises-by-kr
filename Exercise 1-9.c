#include <stdio.h>
/* count lines in input */
int main()
{
    int c, prev;
    prev=0;
    while ((c = getchar()) != EOF) {
        if (c == '\n' || c == ' ' || c == '\t') {
            if (prev==0){
                putchar(c);
            }
            prev=1; 
        } 
        else {
            putchar(c);
            prev=0;
        }
    }
    
    
}