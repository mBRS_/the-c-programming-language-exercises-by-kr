#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */

int getline1(char line[], int maxline);
void copy(char to[], char from[]);
void reverse(char line[], int len,char reversed[] );


/* print the longest input */
int main()
{
    int len;
    char line[MAXLINE];
    char reversed[MAXLINE];

    while ((len = getline1(line, MAXLINE)) > 0){
        reverse(line,len,reversed);
        if (len>0){
            printf("%s", reversed);
        }
    }
    return 0;
    }

/* getline: read a line into s, return length */
int getline1(char s[],int lim)
{
    int c, i;
    for (i=0; i < lim-1 && (c=getchar())!=EOF && c!='\n'; ++i)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i;
    i = 0;
    while ((to[i] = from[i]) != '\0')
    ++i;
}

void reverse(char line[], int len, char reversed[]) {
    reversed[len]='\0';
    reversed[--len]='\n';
    for(int i = 0; len>0; --len) {
        reversed[i]=line[len-1];
        i++;
    }

}