#include <stdio.h>
/* count lines in input */
int main()
{
    int c, prev;
    prev=0;

    while ((c = getchar()) != EOF) {
        if (c == '\t') 
            printf("\\t");
        else if (c == '\b')
            printf("\\b");
        else if (c == '\\')
            printf("\\\\");
        else putchar(c);
    }
    
    
}