#include <stdio.h>
/* copy input to output*/
/*In Linux to give EOF as input in the shell the combination is Ctrl+D*/
int main()
{
int c;

c = getchar() != EOF;
printf("%d", c);

while ((c = getchar()) != EOF)
    putchar(c);

printf("\nValue of EOF: %d\n",EOF);
}

