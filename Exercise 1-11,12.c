#include <stdio.h>
#define IN 1
#define OUT 0

/* count lines, words, and characters in input */

/*Answer to Exercise 1-11: 
The best way to test the word count program is not only with random
inputs, but also trying to analyze the behaviour of the program on
the edge cases. For example testing what happens when there are 
a lot of spaces between 2 words, using rare characters, giving
an empty input etc.

*/

int main()
{
    int c, state, prev;
    state = OUT;
    prev=0;

    while ((c = getchar()) != EOF) {
        if(c==' ' || c=='\t' || c=='\n'){
            if(prev==0) {
                putchar('\n');
                prev=1;
            }
        }
        else {
            putchar(c);
            prev=0;
        }
        
    }
    
}